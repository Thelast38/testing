

const express = require('express');
const router = express.Router();


const pdfMake = require('../pdfmake/build/pdfmake');

const pdfFonts = require('../pdfmake/build/vfs_fonts');

pdfMake.vfs = pdfFonts.pdfMake.vfs;

const date = new Date();
const day = date.getDate();
const month = date.getMonth() + 1;
const year = date.getFullYear();
const allDate = toString(day) + "." + toString(month) + "." + toString(year);




router.post('/pdf', function(req, res, next){

  const fname = req.body.fname;
  const lname = req.body.lname;

  var documentDefine = {
	content: [
		fname,
		lname
	]

  }

  const pdfDoc = pdfMake.createPdf(documentDefine);
  pdfDoc.getBase64(function(data){
    console.log(date);
    res.writeHead(200, {
      "Content-type": "application/pdf",
      "Content-Disposition": 'attachment;filename="BugReport_{date}.pdf"'.replace("{date}", date)
    });

    const download = Buffer.from(data.toString("utf-8"), "base64");
    res.end(download);
  });

});

module.exports = router;
